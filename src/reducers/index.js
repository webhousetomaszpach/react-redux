import { combineReducers } from "redux";
import BooksReducer from "./books";
import ActiveBook from "./active_book";
import Basket from "./basket";

const rootReducer = combineReducers({
    booksList: BooksReducer,
    selectedBook: ActiveBook,
    basket: Basket
});

export default rootReducer;

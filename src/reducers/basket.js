const initialState = {
    booksAdded: [],
    summary: 0,
    summaryOff: 0,
    summaryActual: 0,
    showModal: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case "ADD_TO_BASKET":
            return {
                booksAdded: [...state.booksAdded, action.payload],
                summary: state.summary += action.payload.price,
                summaryOff: action.payload.promo ? parseFloat((state.summaryOff += (action.payload.price - (action.payload.off / 100 * action.payload.price))).toFixed(2)) : state.summaryOff += action.payload.price,
                summaryActual: parseFloat((state.summary - state.summaryOff).toFixed(2)),
                showModal: false
            };
        case "POKAZ_MODAL":
            return {
                ...state,
                showModal: action.payload
            };
    }

    return state;
}
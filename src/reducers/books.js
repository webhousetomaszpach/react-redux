export default function () {
    return [
        {
            title: "Javascript: The Good Parts",
            pages: 172,
            promo: true,
            author: "Douglas Crockford",
            cover: "https://static01.helion.com.pl/global/okladki/326x466/d5b3a7c4633cffff54c886d19779b684,e_2gxo.jpg",
            off: 30,
            price: 69,
            desc: "Most programming languages contain good and bad parts, but JavaScript has more than its share of the bad, having been developed and released in a hurry before it could be refined. This authoritative book scrapes away these bad features to reveal a subset of JavaScript that's more reliable, readable, and maintainable than the language as a whole-a subset you can use to create truly extensible and efficient code."
        },
        {
            title: "Harry Potter and the Philosopher's Stone",
            author: "J.K. Rowling",
            pages: 331,
            cover: "https://img.buzzfeed.com/buzzfeed-static/static/2017-06/28/6/asset/buzzfeed-prod-fastlane-03/sub-buzz-12852-1498644629-3.jpg?output-format=auto&output-quality=auto",
            desc: "Po piętnastu latach od polskiej premiery „Harry’ego Pottera” Pottermaniacy mają powód do świętowania! Długo oczekiwane ilustrowane wydanie pierwszej części cyklu „Harry Potter i Kamień Filozoficzny” ukazuje się z ponad 100 ilustracjami. Ich autorem jest Brytyjczyk Jim Kay, zdobywca nagrody Kate Greenway Medal Jedenastoletni Harry Potter to sierota i podrzutek, który od niemowlęcia wychowywany był przez ciotkę i wuja, którzy traktowali go jak piąte koło u wozu. Pochodzenie chłopca owiane jest tajemnicą; jedyną pamiątką Harry'ego z przeszłości jest zagadkowa blizna na czole. Skąd jednak biorą się niesamowite zjawiska, które towarzyszą nieświadomemu niczego Potterowi? Harry nigdy by się nie spodziewał, że można latać na miotle, znać bardzo pożyteczne zaklęcia i nosić pelerynę niewidkę. Nigdy też nie przyszłoby mu do głowy, że to właśnie on stoczy walkę z potężnym i złym Lordem Voldermortem.",
            promo: true,
            off: 5,
            price: 39
        },
        {
            title: "The Dark Tower",
            pages: 191,
            author: "Nikolaj Arcel",
            cover: "https://www.videoplusbooks.ca/wp-content/uploads/2017/11/The-Dark-Tower-Poster-Idris-Elba-1wzr9td.jpeg",
            promo: true,
            desc: "Adaptacja sagi Stephena Kinga. Roland przemierza powojenny świat zaludniony przez mutanty, demony i wampiry poszukując tajemniczej Mrocznej Wieży.",
            off: 15,
            price: 129
        },
        {
            title: "Eloquent Ruby",
            pages: 832,
            promo: false,
            off: 20,
            price: 39,
            author: "Russ Olsen",
            cover: "https://images.gr-assets.com/books/1347345993l/2278064.jpg",
            desc: "It’s easy to write correct Ruby code, but to gain the fluency needed to write great Ruby code, you must go beyond syntax and absorb the “Ruby way” of thinking and problem solving. In Eloquent Ruby, Russ Olsen helps you write Ruby like true Rubyists do–so you can leverage its immense, surprising power."
        }
    ]
}
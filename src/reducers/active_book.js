// State argument is not application state, only the state
// this reducer is responsible for

const initialState = {
    bookSelected: null
};

export default function(state = initialState, action) {
    switch(action.type) {
        case "BOOK_SELECTED":
            return {
                bookSelected: action.payload
            };
    }

    return state;
}
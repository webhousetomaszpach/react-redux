import React, { Component } from "react";

import BookList from "../containers/book-list";
import BookDetail from "../containers/book-detail";
import Basket from "../containers/basket";

export default class App extends Component {
    render() {
        return (
            <div>
                <Basket/>
                <BookList/>
                <BookDetail/>
            </div>
        );
    }
}

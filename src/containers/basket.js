import React, { Component } from "react";
import { connect } from "react-redux";
import { toggleModal } from "../actions";
import { bindActionCreators } from "redux";

class Basket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            emptyIco: 'https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/shopping_basket_full.png',
            notEmptyIco: 'https://www.shareicon.net/data/128x128/2016/09/10/828150_store_512x512.png'
        }
    }

    basketIco(book) {
        return (
            <div id="basket-icon" onClick={() => this.props.toggleModal(!this.props.basket.showModal)}>
                <img
                    src={book.length > 0 ? this.state.emptyIco : this.state.notEmptyIco}
                    alt="basket_icon"/>
            </div>
        )
    }

    modalShadow() {
        return <div id="modal-shadow" onClick={() => this.props.toggleModal(!this.props.basket.showModal)}/>
    }

    listItems() {
        return this.props.basket.booksAdded.map((book, index) => {
            return (
                <li key={book.title + index}>{book.title} - {(book.price - (book.off / 100 * book.price)).toFixed(2)} zł</li>
            )
        })
    }

    basketModal(book) {
        return (
            <div>
                {this.modalShadow()}
                <div id="basket-modal">
                    <div className="close" onClick={() => this.props.toggleModal(!this.props.basket.showModal)}>x</div>
                    <h3>Twój koszyk ({book.length}):</h3>
                    {this.listItems()}

                    <div className="summary row">
                        <div className="details col-sm-6">
                            <div>Standardowa cena książek: {this.props.basket.summary} zł</div>
                            <div>Łącznie: {(this.props.basket.summaryOff).toFixed(2)} zł</div>
                            <div>Oszczędzasz: {(this.props.basket.summaryActual).toFixed(2)} zł</div>
                        </div>

                        <div className="actions col-sm-6">
                            <button>kup</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        console.log('Show basket modal: ' + this.props.basket.showModal);
        return (
            <div id="basket" className="container-fluid">
                {this.basketIco(this.props.basket.booksAdded)}
                {this.props.basket.showModal ? this.basketModal(this.props.basket.booksAdded) : null}
            </div>
        )
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        basket: state.basket
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({toggleModal}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
import React, { Component } from "react";
import { connect } from "react-redux";
import { selectBook } from "../actions/index";
import { bindActionCreators } from "redux";

class BookList extends Component {
    price(book) {
        return (
            <div>{book.promo ? (
                <div>
                    <div>Cena: {(book.price - (book.off / 100 * book.price)).toFixed(2)} zł</div>
                    - <span style={{color: "red"}}>promocja {book.off}%</span></div>
            ) : (
                <div>Cena: {book.price} zł</div>
            )}
            </div>
        )
    }

    renderList() {
        return this.props.books.map((book) => {
            return (
                <li
                    key={book.title}
                    onClick={() => {
                        this.props.selectBook(book);
                    }}
                    className="list-group-item"
                    style={{cursor: 'pointer'}}
                >
                    {book.title}
                    <div>Stron: {book.pages}</div>
                    <div>{this.price(book)}</div>
                </li>
            )
        })
    }

    render() {
        return (
            <ul className="list-group col-sm-3">
                {this.renderList()}
            </ul>
        )
    }
}

function mapStateToProps(state) {
    // Whatever is returned will displayModal up as props
    // inside of BookList
    return {
        books: state.booksList
    }
}

// Anything returned from this function will end up as props
// on the BookList container
function mapDispatchToProps(dispatch) {
    // Whenever selectBook is called, the results should be passed
    // to all of our reducers
    return bindActionCreators({
        selectBook: selectBook
    }, dispatch)
}

// Promote BookList from a component to a container - it needs to know
// about this new dispatch method, selectBook. Make it available
// as a prop.
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
import React, { Component } from "react";
import { addBookToBasket } from "../actions/index";
import { connect } from "react-redux";
import { removeBookFromBasket } from "../actions";

class BookDetail extends Component {
    author(book) {
        return (
            book.author ? (
                <h6>Autor: <b>{book.author}</b></h6>
            ) : null
        )
    }

    price(book) {
        return (
            <div className="m-b-1">{book.promo ? (
                <div>
                    <del>{book.price} zł</del>
                    - <span style={{color: "red"}}>promocja {book.off}%</span>
                    <h5>{this.isPromo(book)}</h5>
                </div>
            ) : (
                <div>{book.price} zł</div>
            )}
            </div>
        )
    }

    isPromo(book) {
        return (
            <div><b>{(book.price - (book.off / 100 * book.price)).toFixed(2)} zł</b> - taniej o {(book.off / 100 * book.price).toFixed(2)} zł.</div>
        )
    }

    cover(book) {
        return (
            book.cover ? (
                <img style={{maxWidth: "100%"}} src={this.props.book.cover} alt={this.props.book.title}/>
            ) : null
        )
    }

    addToBasketButton(book) {
        return (
            <button
                onClick={ () => this.props.addBookToBasket(book) }
                style={{border: "none", backgroundColor: "red", color: "#fff", padding: "5px 15px"}}
            >Dodaj do koszyka</button>
        )
    }

    remove(book) {
        return (
            <button
                onClick={ () => this.props.removeBookFromBasket(book) }
                style={{border: "none", backgroundColor: "red", color: "#fff", padding: "5px 15px"}}
            >usun</button>
        )
    }

    description(book) {
        return (
            book.desc ? (
                <p>{book.desc}</p>
            ) : null
        )
    }

    render() {
        let book = this.props.book;

        if (!book) {
            return <h2>Wybierz książkę po lewej, aby zobaczyć więcej szczegółów.</h2>;
        }


        return (
            <div className="col-sm-9">
                <div className="details m-b-2" style={{borderBottom: "1px solid #000"}}>
                    <h2>{book.title}</h2>
                    <div className="row m-b-2">
                        <div className="left col-sm-3">
                            {this.cover(book)}
                        </div>
                        <div className="right col-sm-9">
                            {this.author(book)}
                            {this.price(book)}
                            {this.addToBasketButton(book)}
                        </div>
                    </div>
                </div>
                {this.description(book)}
            </div>
        )
    }
}

function mapStateToProps(state) {
    // Whatever is returned will displayModal up as props
    // inside of BookList
    return {
        book: state.selectedBook.bookSelected
    }
}

// Anything returned from this function will end up as props
// on the BookList container
function mapDispatchToProps(dispatch) {
    // Whenever selectBook is called, the results should be passed
    // to all of our reducers
    // return bindActionCreators({ addToBasket: addToBasket2 }, dispatch)
    return {
        addBookToBasket: (item) => dispatch(addBookToBasket(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
export function selectBook(book) {
    // selectBook is an ActionCreator, it needs to return an action,
    // an object with a type property.
    return {
        type: "BOOK_SELECTED",
        payload: book
    };
}

export function addBookToBasket(book) {
    console.log('ACTION, index.js - Dodaję ' + book.title + ' do koszyka.');
    return {
        type: "ADD_TO_BASKET",
        payload: book
    }
}

export function removeBookFromBasket(book) {
    console.log('ACTION, index.js - Usuwam ' + book.title + ' z koszyka.');
    return {
        type: "REMOVE_FROM_BASKET",
        payload: book
    }
}

export function toggleModal(toogle) {
    // console.log(toogle)
    return {
        type: "POKAZ_MODAL",
        payload: toogle
    }
}